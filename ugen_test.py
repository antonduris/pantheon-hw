import unittest
# import ugen
from ugen import *


class TestSum(unittest.TestCase):

    def test_username(self):
        case1 = make_username('1234:Jozef:Miloslav:Hurban:Legal')
        result1 = 'jmhurban' 
        self.assertEqual(case1, result1, 'must be jmhurban')

        case2 = make_username('4567:Milan:Rastislav:Stefanik:Defence')
        result2 = 'mrstefan' 
        self.assertEqual(case1, result1, 'must be mrstefan')

        case3 = make_username('4563:Jozef::Murgas:Development')
        result3 = 'jmurgas' 
        self.assertEqual(case1, result1, 'must be jmurgas')

        case4 = make_username('1111:Pista::Hufnagel:Sales')
        result4 = 'phufnage' 
        self.assertEqual(case1, result1, 'must be phufnage')

if __name__ == '__main__':
    unittest.main()
