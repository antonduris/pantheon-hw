import argparse

# define terminal options
def options():
    text = 'Program for generating a list of usernames.'
    parser = argparse.ArgumentParser(description=text)
    parser.add_argument('-o', '--output',
                        help='Processes all input files and writes the generated data to output file',
                        nargs = '*')
    return parser

# checks if there are at least 2 arguments
def check_args(parser):
    if len(args.output) < 2:
        parser.print_help()
    else:
        pass

# make a list of datas from string(inout text files)
def split_data(line):
    return list(line.rstrip('\n').split(':'))

# make username form list, first 8 characters
def make_username(line):
    #just for test 
    if 'username' in globals():
        pass
    else:
        global usernames
        global repeating
        repeating = []
        usernames = []
    # data list have all info. from txt file
    data = split_data(line)
    try:
        username = (data[1][0] + data[2][0] + data[3]).lower()
    except:
        username = (data[1][0] + data[3]).lower() 
    username = username[0:8]
    username = check_username(username)
    return username

# checking for repeating username, adding number to repeating usernames
def check_username(username):
    if username in usernames:
        repeating.append(username)
        username = username + str(repeating.count(username))
    else:
        usernames.append(username)
    return username

if __name__ == '__main__':
    global usernames
    global repeating
    repeating = []
    usernames = []

    parser = options()
    args = parser.parse_args()
    if args.output:
        check_args(parser)
        # open output file for writing
        with open(args.output[0],'w') as output_file:
            for txt_file in args.output[1:]:
                # open all input files
                with open(txt_file,'r') as input_file:
                    for line in input_file: 
                        data = split_data(line)
                        username = make_username(line)
                        # inserting username to data list
                        data.insert(1,username)
                        # writing data to output file
                        for index,record_item in enumerate(data):
                            output_file.write(record_item)
                            if index < len(data)-1:
                                output_file.write(':')
                        output_file.write('\n')
                input_file.close()
        output_file.close()

